[![pipeline status](https://gitlab.com/ymasson-salt/ci-kitchen-workers/badges/devel/pipeline.svg)](https://gitlab.com/ymasson-salt/ci-kitchen-workers/commits/devel)


[![pipeline status](https://gitlab.com/ymasson-salt/ci-kitchen-workers/badges/master/pipeline.svg)](https://gitlab.com/ymasson-salt/ci-kitchen-workers/commits/master)


Custom images for Saltstack CI, with systemd workaround. Used by https://gitlab.com/ymasson-salt/ci-kitchen
